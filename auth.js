const e = require('express');
const jwt = require ('jsonwebtoken');
const secret = "ecommerceapi"


module.exports.createAccessToken = (user) =>{

    // this generates a token
    let payload = {
        id : user._id,
        email : user.email,
        isAdmin : user.isAdmin
    }

    return jwt.sign(payload,secret);

}


module.exports.verify = (req,res,next) =>{

    let token = req.headers.authorization

    if (typeof token === "undefined") {
        res.send ("auth:failed")
    } else {
        
        //extract token and remove Bearer from the token info on terminal
        token =  token.slice(7, token.length)

        console.log(token)


        //this is to verify token
        //shhhhh contains "secret" variable

        jwt.verify(token, secret, function (err, decoded) {

            if (err){
                res.send ({auth : "failed"})
            } else {
                req.user = decoded
                next()
            }
        });
    }

}


module.exports.verifyAdmin = (req,res,next) =>{

    console.log(req.user)


    if (req.user.isAdmin){
        next ()
    } else {
        res.send(false)
    }
    
}





module.exports.verifyNonAdmin = (req,res,next) =>{

    console.log(req.user)


    if (!req.user.isAdmin){
        next ()
    } else {
        res.send(false)
    }
    
}
