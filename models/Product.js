const mongoose = require ('mongoose');


const ProductSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},

	description: {
		type: String,
		required: true
	},

	price: {
		type: Number,
		required: true
	},

	isActive: {
		type: Boolean,
		default: true
	}
	

},{timestamps : {createdAt : 'addedOn'}});


module.exports = mongoose.model ('Product', ProductSchema);