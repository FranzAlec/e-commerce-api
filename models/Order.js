const mongoose = require ('mongoose');






const OrderSchema = new mongoose.Schema ({

	userId : {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: true,
	},

	totalAmount : {
		type: Number,
		default: 0

	},

	products: [{

			productId : {
				type: mongoose.Schema.Types.ObjectId,
				ref: "Product",
				required: true,
			},

			quantity : {
				type : Number,
				required : true,
				default: 1
			},

			price : {
				type: Number,
				
			},

			subTotal : {
				type : Number,
				default : 0
			}
	}],


},{timestamps : {createdAt : 'purchasedOn'}});




module.exports = mongoose.model ("Order", OrderSchema);