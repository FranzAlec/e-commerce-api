const router = require ('express').Router();

const Order = require ('./../models/Order.js')

const {order, allOrder,myOrder} = require('./../controllers/orderController');

const { verify, verifyAdmin, verifyNonAdmin } = require ('./../auth');



router.post ('/createorder',verify, verifyNonAdmin, order)

router.get ('/all',verify ,verifyAdmin ,allOrder)


router.get ('/myorder',verify ,verifyNonAdmin ,myOrder)

module.exports = router;