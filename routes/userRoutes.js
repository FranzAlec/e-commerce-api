const router = require ('express').Router();
const { register, login, setAdmin, editProfile, getAllUser} = require('./../controllers/userController');
// const { order } = require ('./../controllers/orderController.js')
const { verify, verifyAdmin, verifyNonAdmin } = require ('./../auth');

router.post ('/register', register);

router.post ('/login', login);

router.get ('/getdetails', getAllUser); //for checking purposes

router.put ('/:userId/updateprofile', verify, verifyNonAdmin, editProfile);

router.put ('/:userId', verify, verifyAdmin, setAdmin);


module.exports = router;