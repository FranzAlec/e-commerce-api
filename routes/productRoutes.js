const router = require ('express').Router();

const { 
	getAllProd,
	getSingleProd, 
	addProd, 
	updateProd, 
	archiveProd,
	getAll 
} = require('./../controllers/productController');





const { verify, verifyAdmin } = require ('./../auth');

router.get ('/', getAllProd);

router.post ('/', verify ,verifyAdmin , addProd);




router.get ('/all', getAll);






router.get ('/:productId', getSingleProd);

router.put ('/:productId',verify, verifyAdmin, updateProd);

router.put ('/:productId/archive', verify ,verifyAdmin , archiveProd);


module.exports = router;