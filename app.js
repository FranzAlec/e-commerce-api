//required modules
const express = require ("express");
const mongoose = require("mongoose");


//initialized express
const app = express()

//setting port
const port = process.env.PORT || 4000;

//database connection
mongoose.connect ('mongodb+srv://first-alec:admin@cluster0.krydg.mongodb.net/e-commerce-api?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}).then(() =>{
    console.log("We are connected to database")
}).catch((err) =>{
    console.log(err.message)
})

//req.body parser
app.use(express.json())


//define routes
const userRoutes = require ('./routes/userRoutes')
app.use ('/users', userRoutes)

const productRoutes = require ('./routes/productRoutes')
app.use ('/products', productRoutes)

const orderRoutes = require ('./routes/orderRoutes')
app.use ('/orders', orderRoutes)



//port assignement
app.listen (port, () =>{
    console.log(`Listening to port: ${port}`)
})