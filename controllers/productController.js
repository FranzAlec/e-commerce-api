const Product = require ('./../models/Product')

//product allActive list
//@GET
//privacy public
module.exports.getAllProd = (req,res) =>{

    Product.find({isActive : true})
    .then(product =>{
        res.send (product)
    }) .catch (err =>{
        res.send(err)
    })
}



//get all non active and active products

module.exports.getAll = (req,res) =>{

   Product.find({})
    .then(product =>{
        res.send (product)
    }) .catch (err =>{
        res.send(err)
    })

}



//product single list
//@GET
//privacy public
module.exports.getSingleProd = (req,res) =>{

    Product.findById (req.params.productId)
    .then (prod =>{
        console.log(prod)
        return res.send(prod)
    }).catch (err =>{
        res.send(err)
    })
}



//create a product
//@POST
//privacy private
module.exports.addProd = (req,res)=>{
	
	let newProduct = new Product ({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	})

	//this is used to save our new product to our database
	newProduct.save()
	.then(() => res.send(true))
	.catch(() => res.send(false))



}

//update product information
//@PUT
//privacy private
module.exports.updateProd = (req,res)=>{
	
	//this will contain the updates to our course
	let updatedProd = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

    
	// this will process the updates
	//.findByIdAndUpdate(req.params.courseId) will look for the particular course/docu and will apply the update on the matched docu
	//Modelname..findByIdAndUpdate(id, update)
	Product.findByIdAndUpdate(req.params.productId, updatedProd, {new: true})
	.then(product=>{
		res.send(true)
	})
	.catch(()=>{
		res.send(false)
	})
}




//archive product information
//@GET
//privacy private
module.exports.archiveProd = (req,res)=>{

	let updateProduct = {
		isActive: false 
	}
	
	Product.findByIdAndUpdate(req.params.productId, updateProduct)
	.then(()=>res.send(true))
	.catch(()=>res.send(false))
}


