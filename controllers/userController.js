const bcrypt = require ('bcrypt')
const { createAccessToken } = require('../auth')
const User = require ('./../models/User')
const Product = require ('./../models/Product')


//user registration
//@POST
//privacy public

module.exports.register = (req,res) =>{

    //password validation (min = 8)

    if(req.body.password.length <8) return res.send('Password too short!')

    //password confirmation

    if (req.body.password !== req.body.confirmPassword) return res.send('Passsword not matched!')


    //password hash/encryption
    const hash = bcrypt.hashSync(req.body.password, 9);


    //reassigning the value of req.body.password to the declared variable hash

    req.body.password = hash;

    let newUser = new User ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        mobileNo: req.body.mobileNo
    })

    newUser.save()
    .then(() => res.send('Successfully registered!'))
    .catch(() => res.send('Email is already taken!'))
}






module.exports.login = (req,res) =>{

    User.findOne({email : req.body.email})
    .then (user => {
        

        if (!user){
            res.send ("Email or Password incorrect")
        } else {

            //declaration of matched password from the request body password to the password on DB
            let matchedPW = bcrypt.compareSync(req.body.password, user.password);

            if (!matchedPW) {

                res.send("Email or Password incorrect")
            } else {

                res.send({access : createAccessToken(user)})
            }
        }
    })
}






module.exports.setAdmin = (req,res) =>{

    let updatedUserAdmin = {
        isAdmin: true
    } 

    User.findByIdAndUpdate(req.params.userId, updatedUserAdmin)
    .then (() => res.send (true))
    .catch (() => res.send (false))
}




// module.exports.orderProduct = (req,res) =>{

//     User.findById(req.user.id,{password: 0})

//     .then(user =>{
//         user.orders.push(req.body)
//         return user.save()
//     })

//     .then(() =>{
//         return Product.findById(req.body.productId)
//     })
//     .then(order =>{
//         product.purchased.push({userId: req.user.id})
//         return order.save()
//     })
//     .then(checkout =>{
//         res.send(checkout)
//     })
//     .catch(err =>{
//         res.send(err)
//     })

module.exports.editProfile = (req,res) =>{

    // let updatedUserProfile = {
    //     firstName: req.body.firstName,
    //     lastName: req.body.lastName,
    //     email: req.body.email,
    //     password: req.body.password,
    //     mobileNo: req.body.mobileNo
    // } 

    User.findByIdAndUpdate(req.params.userId, req.body, {new: true})
    .then (updated =>{
     res.send (updated)
 })
    .catch (err =>{ 
     res.send (false)
    })
}




module.exports.getAllUser = (req,res) =>{

    User.find({}, {password:0})
    .then(user =>{
        res.send (user)
    }) .catch (err =>{
        res.send(err)
    })
}