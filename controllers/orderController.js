const Order = require ('./../models/Order');
const Product = require ('./../models/Product');
const User = require ('./../models/User');



module.exports.order = (req,res) =>{


	Order.create({
		userId : req.user.id,
		productId : req.body.productId,
		quantity : req.body.quantity
		
	})
	.then(()=>{
		return Order.findOne({userId : req.user.id}) //accessed the ordertable with the id of the user
	})
	.then((order)=>{
		console.log(order)
		Product.findById(req.body.productId)
		.then(product =>{

			
			let subTotal = product.price * req.body.quantity
			order.totalAmount += subTotal
			order.products.push({
				productId: req.body.productId,
				quantity : req.body.quantity,
				subTotal : subTotal
			})
			console.log(order.totalAmount)
			order.save()
			.then ((updatedorders)=>{
				res.send(updatedorders)
			})
		})
	},{new : true})
	.catch(err => res.status(404).send(err))
}
	

	

	// Order.products.push()
	// .then (order =>{
	// 	// user.products.push(req.body)
	// 	res.send(order)
	// })

	// .catch (err=>{
	// 	res.send(err)
	// })


	// Order.create({
	// 	userId: req.user.id,
	// 	productId: req.body.productId
	// })
	// .then (order=>{
	// 	Order.findOneAndUpdate ({userId: req.user.Id}, {totalAmount:1000}, {new: true})
	// 	.then(updatedOrder =>{
	// 		console.log(updatedOrder)
	// 		res.send(updatedOrder)
	// 	})
	// 	.catch(err =>{
	// 		res.send(err)
	// 	})
	// })




module.exports.allOrder = (req,res) =>{

	Order.find()
	.then (allorders =>{
		res.send(allorders)
	})
	.catch(err =>{
		res.send(err)
	})
}









module.exports.myOrder = (req,res) =>{
	// console.log(req.user.id)
    Order.find({userId: req.user.id})
    .then (myorders =>{
        console.log(myorders)
        res.send(myorders)
    })
    .catch(err =>{
        res.send(err)
    })
}